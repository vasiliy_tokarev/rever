# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(id: 1, email: "user1@rever.co", :password => 'password', :password_confirmation => 'password')
User.create(id: 2, email: "user2@rever.co", :password => 'password', :password_confirmation => 'password')
User.create(id: 3, email: "user3@rever.co", :password => 'password', :password_confirmation => 'password')
User.create(id: 4, email: "user4@rever.co", :password => 'password', :password_confirmation => 'password')
User.create(id: 5, email: "user5@rever.co", :password => 'password', :password_confirmation => 'password')
User.create(id: 6, email: "user6@rever.co", :password => 'password', :password_confirmation => 'password')
User.create(id: 7, email: "user7@rever.co", :password => 'password', :password_confirmation => 'password')
User.create(id: 8, email: "user8@rever.co", :password => 'password', :password_confirmation => 'password')
User.create(id: 9, email: "user9@rever.co", :password => 'password', :password_confirmation => 'password')
User.create(id: 10, email: "user10@rever.co", :password => 'password', :password_confirmation => 'password')

Subscription.create(premium_plan_id: 1, start_date: "2015-05-16", end_date: "2015-07-16", user_id: 1)
Subscription.create(premium_plan_id: 2, start_date: "2015-02-10", end_date: "2015-04-10", user_id: 1)
Subscription.create(premium_plan_id: 2, start_date: "2014-01-10", end_date: "2014-10-10", user_id: 2)
Subscription.create(premium_plan_id: 2, start_date: "2015-05-10", end_date: "2015-09-10", user_id: 2)
Subscription.create(premium_plan_id: 2, start_date: "2015-03-02", end_date: "2015-04-02", user_id: 3)
Subscription.create(premium_plan_id: 1, start_date: "2015-02-01", end_date: "2015-04-01", user_id: 4)
Subscription.create(premium_plan_id: 1, start_date: "2015-06-10", end_date: "2015-07-10", user_id: 5)
Subscription.create(premium_plan_id: 1, start_date: "2015-03-11", end_date: "2015-09-11", user_id: 6)
Subscription.create(premium_plan_id: 2, start_date: "2015-05-05", end_date: "2015-07-05", user_id: 7)
Subscription.create(premium_plan_id: 1, start_date: "2015-06-11", end_date: "2015-08-11", user_id: 7)
Subscription.create(premium_plan_id: 1, start_date: "2015-05-12", end_date: "2015-09-12", user_id: 7)

PremiumPlan.create(id: 1, title: "Apple", price_per_month: 4.20, active: true)
PremiumPlan.create(id: 2, title: "Stripe", price_per_month: 5.99, active: true)

AdminUser.create!(:email => 'admin@example.com', :password => 'password', :password_confirmation => 'password')